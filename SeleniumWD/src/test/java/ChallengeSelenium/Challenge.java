package challenge;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Challenge {


    private WebDriver driver;
    private ArrayList<Map<String,String>> listJdd;
    private Map<String,String> data;
    private String firstName,lastName,companyName,RoleInCompany,address,email,phoneNumber;
    String champ_First_Name= "//input[@ng-reflect-name='labelFirstName']";
    String champ_Last_Name= "//input[@ng-reflect-name='labelLastName']";
    String champ_Campany_Name= "//input[@ng-reflect-name='labelCompanyName']";
    String champ_Adresse= "//input[@ng-reflect-name='labelAddress']";
    String champ_labelRole= "//input[@ng-reflect-name='labelRole']";
    String champ_labelEmail= "//input[@ng-reflect-name='labelEmail']";
    String champ_labelPhone= "//input[@ng-reflect-name='labelPhone']";

    public ArrayList<Map<String, String>> loadCsvJDD (String fileName) throws IOException {
        String csvFilePath = "src/main/ressources/" + fileName + ".csv";
        ArrayList<Map<String, String>> listJDD = new ArrayList<>();
        List<String[]> list =
                Files.lines(Paths.get(csvFilePath))
                        .map(line -> line.split("\\\\r?\\\\n"))
                        .collect(Collectors.toList());
        for (int j = 1; j < list.size(); j++) {
            Map<String, String> jdd = new HashMap<>();
            String[] titres = list.get(0)[0].split(",");
            String[] val = list.get(j)[0].split((","));
            for (int i = 0; i < titres.length; i++) {
                jdd.put(titres[i], val[i]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }

    // Méthode pour ecrire
    public void inpuMethod(String xpath,String var) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.sendKeys(var);
    }

    // Méthode pour cliquer
    public void clickMethod(String xpath)  {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }


    @Test
    public void testChallenge() {
        System.setProperty("webdriver.gecko.driver", "src/test/ressources/driver/geckodriver.exe");
//                FirefoxOptions options = new FirefoxOptions();
//        options.setHeadless(true);
        driver = new FirefoxDriver();


        driver.get("https://www.rpachallenge.com/");
        driver.manage().window().maximize();
        WebElement start = driver.findElement(By.xpath("//button[@class='waves-effect col s12 m12 l12 btn-large uiColorButton']"));
        start.click();


        /* On recup l'utilisateur pour se log */
        try {
            listJdd= loadCsvJDD("csv");
            for(int i=0; i<10;i++)
            {
                data = listJdd.get(i);
                firstName = data.get("First Name");
                lastName = data.get("Last Name");
                companyName = data.get("Company Name");
                RoleInCompany = data.get("Role in Company");
                address = data.get("Address");
                email = data.get("Email");
                phoneNumber = data.get("Phone Number");


                inpuMethod(champ_First_Name,firstName);
                inpuMethod(champ_Last_Name,lastName);
                inpuMethod(champ_Campany_Name,companyName);
                inpuMethod(champ_labelRole,RoleInCompany);
                inpuMethod(champ_Adresse,address);
                inpuMethod(champ_labelEmail,email);
                inpuMethod(champ_labelPhone,phoneNumber);
                WebElement submit = driver.findElement(By.xpath("//input[@type='submit']"));
                submit.click();

            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
