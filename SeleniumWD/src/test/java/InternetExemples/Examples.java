package InternetExemples;

import com.sun.xml.internal.ws.wsdl.writer.document.StartWithExtensionsType;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.*;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.TakesScreenshot;

import javax.xml.bind.Element;

import static org.junit.Assert.*;

public class Examples {

    private WebDriver driver;
    private String emailTest = "oui@gmail.com";
    private String checkboxXpath = "//a[@href='/checkboxes']";
    private String mdpoublieXpath = "//a[@href='/forgot_password']";
    private String horizontalXpath = "//a[@href='/horizontal_slider']";



    //méthode pr cliquer et mettre en surbrillance
    public void myClick(WebElement element) throws InterruptedException {
        JavascriptExecutor js=(JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        Thread.sleep(1500);
        element.click();

    }
    //méthode pr écrire et mettre en surbrillance
    public void inputText(WebElement element, String text) throws InterruptedException {
        JavascriptExecutor js=(JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();",element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        Thread.sleep(1000);
        element.sendKeys(text);
    }
    //méthode screenshot
    public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

        TakesScreenshot scrShot =((TakesScreenshot)webdriver);

        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

        File DestFile=new File(fileWithPath);

        FileUtils.copyFile(SrcFile, DestFile);
    }
    //méthode pr cliquer sur des exos
    public void CliquerSurExo(String xpath) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        myClick(element);
    }
//    public WebElement hhh(String txt){
//        WebElement cc =driver.findElement(By.xpath(txt));
//        return cc;
//    }

    @Before
    public void creaDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://the-internet.herokuapp.com/");
        driver.manage().window().maximize();
    }

    @Test
    public void Checkboxes() throws InterruptedException {

        CliquerSurExo(checkboxXpath);
        WebElement checkbox1 = driver.findElement(By.xpath("//form[@id='checkboxes']/input[1]"));
        WebElement checkbox2 = driver.findElement(By.xpath("//form[@id='checkboxes']/input[2]"));

        //assertion la checkbox 2 est cochée par défaut
        assertTrue(checkbox2.isSelected());

        //cliquer sur checkbox 1 puis vérifier qu'elle est cochée
        myClick(checkbox1);
        assertTrue(checkbox1.isSelected());

        //cliquer sur checkbox 2 puis vérifier qu'elle est décochée
        myClick(checkbox2);
        assertFalse(checkbox2.isSelected());
    }


    @Test
    public void ForgotPassword() throws Exception {
        CliquerSurExo(mdpoublieXpath);

//        //rentre un email dans la case email
//        webelement("//input[@name='email']");

        WebElement caseEmail = driver.findElement(By.xpath("//input[@name='email']"));
        inputText(caseEmail, emailTest);

        //clique sur le bouton "retrieve mot de passe"
        WebElement retrouverMdp = driver.findElement(By.xpath("//i[@class='icon-2x icon-signin']"));
        myClick(retrouverMdp);

        //si la page contient internal server error, prendre un screenshot, sinon ne rien faire
        if (driver.getPageSource().contains("<h1>Internal Server Error</h1>")) {
            takeSnapShot(driver, "src/main/ressources/screenshot/passworderror.jpg");
        }
    }

    @Test

    public void HorizontalSlider() throws InterruptedException {
        CliquerSurExo(horizontalXpath);

        //vérifier que le curseur est à 0 au début
        WebElement curseurInitial = driver.findElement(By.xpath("//input[@type='range']"));
        WebElement valeurInitiale = driver.findElement(By.id("range"));
        assertEquals("0", valeurInitiale.getText());

        //déplacer le curseur à 3
        Actions a = new Actions(driver);
            //WebElement curseura1 = driver.findElement(By.xpath("//input[@value='1']"));
            //a.dragAndDrop(curseurInitial, curseura1).build().perform();
        a.click(curseurInitial).sendKeys(Keys.ARROW_RIGHT).perform();

        //vérifier que la valeur du curseur est à 3
        assertEquals("3", valeurInitiale.getText());

    }

}
