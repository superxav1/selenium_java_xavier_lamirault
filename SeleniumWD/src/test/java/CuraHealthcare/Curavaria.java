package CuraHealthcare;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Curavaria {

    private WebDriver driver;
    private WebDriverWait wait;
    private WebElement boutonConnexion, login, password, boutonLogin, menuLateral, logOut, explicitWait;

    // XPath variables
    private String xpathBtnMakeAppointment = "//*[@id='btn-make-appointment']";
    private String xpathUsername = "//*[@name='username']";
    private String xpathPassword = "//*[@name='password']";
    private String xpathBtnLogin = "//*[@type='submit']";
    private String xpathMenuToggle = "//*[@id='menu-toggle']";
    private String xpathLogout = "//*[@href='authenticate.php?logout']";

    @Before
    public void openWindow() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        // Implicit wait
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://katalon-demo-cura.herokuapp.com/");
        driver.manage().window().maximize();
    }

    @Test
    public void connexionUtilisateur() {
        clickElement(xpathBtnMakeAppointment);
        login("John Doe", "ThisIsNotAPassword");
        clickElement(xpathBtnLogin);
        clickElement(xpathMenuToggle);
        clickElement(xpathLogout);
    }

    private void clickElement(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    private void login(String username, String userPassword) {
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("btn-make-appointment")));

        setInputValue(xpathUsername, username);
        setInputValue(xpathPassword, userPassword);
    }

    private void setInputValue(String xpath, String value) {
        driver.findElement(By.xpath(xpath)).sendKeys(value);
    }
}
