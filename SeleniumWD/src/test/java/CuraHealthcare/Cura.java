package CuraHealthcare;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Cura {

    private WebDriver driver;
    private WebDriverWait wait;
    private WebElement boutonConnexion, login, password, boutonLogin, menuLateral, logOut, explicitWait;

    @Before
    public void openWindow() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
//        Implicit wait
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://katalon-demo-cura.herokuapp.com/");
        driver.manage().window().maximize();
    }


    @Test
    public void connexionUtilisateur() {

        //Cliquer sur le bouton "Make Appointment"
        WebElement boutonConnexion = driver.findElement(By.xpath("//*[@id='btn-make-appointment']"));
        boutonConnexion.click();

        //Se connecter avec un login mot de passe
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("btn-mak-appointment")));
        login = driver.findElement(By.xpath("//*[@name='username']"));

        login.sendKeys("John Doe");
        password = driver.findElement(By.xpath("//*[@name='password']"));
        password.sendKeys("ThisIsNotAPassword");

        //Cliquer sur le bouton Login
        boutonLogin = driver.findElement(By.xpath("//*[@type='submit']"));
        boutonLogin.click();

        // Se déconnecter
        menuLateral = driver.findElement(By.id("menu-toggle"));
        menuLateral.click();
        logOut = driver.findElement(By.xpath("//*[@href='authenticate.php?logout']"));
        logOut.click();
    }
}


////    @After
////    public void Quitter() {
////        driver.quit();
////    }
