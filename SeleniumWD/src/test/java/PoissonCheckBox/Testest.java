package PoissonCheckBox;

import com.sun.xml.internal.ws.wsdl.writer.document.StartWithExtensionsType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.*;

import static org.junit.Assert.*;

public class Testest {

    private WebDriver driver;
    private WebDriverWait wait;
    private String nomDuFichier = "jdd";

    //    private WebElement boutonConnexion, login, password, boutonLogin, boutonFish,

    @Before
    public void creaDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
    }

    @Test
    public void connexionUtilisateur() throws IOException, InterruptedException {

        //récupérer mdp + pwd de la méthode loadCsvJDD
        ArrayList<Map<String, String>> dataList = loadCsvJDD(nomDuFichier);
        Map<String, String> data = dataList.get(0);

        String log = data.get("login");
        String mdp = data.get("password");

        //CLIQuer sur le botuon sign in
        WebElement boutonConnexion = driver.findElement(By.xpath("//div[@id='MenuContent']/a[text()='Sign In']"));
        myClick(boutonConnexion);

        WebElement login = driver.findElement(By.xpath("//input[@name='username']"));
        myClick(login);
        inputText(login, log);

        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        myClick(password);
        password.clear();
        inputText(password, mdp);

        WebElement boutonLogin = driver.findElement(By.xpath("//*[@name='signon']"));
        myClick(boutonLogin);


        //Accéder à la page chien
        WebElement categorieDogs = driver.findElement(By.xpath("//area[@alt ='Dogs']"));
        myClick(categorieDogs);
//
//        //vérifier que l'on a accès à la page des chiens
//        WebElement assertDogs = driver.findElement(By.xpath("//a[@href='/actions/Catalog.action?viewProduct=&productId=K9-BD-01']"));
//        assertEquals("K9-BD-01", assertDogs.getText());
//
        //parcourir le tableau pour trouver Dalmation
        retournerNumeroDeLigne("Dalmation");
        WebElement trouverChien = driver.findElement(By.xpath("//*[@id='Catalog']/table/tbody/tr["+retournerNumeroDeLigne("Dalmation")+"]/td[1]"));
        myClick(trouverChien);
        System.out.println(loadCsvJDD(nomDuFichier));

        //get le texte "Dalmation"
        String Dalmation = driver.findElement(By.xpath("//h2[contains(text(), 'Dalmation')]")).getText();
        textFileWriting("src/main/ressources/JDD/csv/dalmation.txt", Dalmation);

        //surbrillance des éléments



    }
    //parcourir le tableau
    public int retournerNumeroDeLigne(String s){
        int ligneCourante = 1;
        List<WebElement> l_lignes = driver.findElements(By.xpath("//*[@id='Catalog']/table/tbody/tr"));
        for(WebElement ligne : l_lignes){
            List<WebElement> l_cell = ligne.findElements(By.xpath("td"));
            for(WebElement cell:l_cell){
                if(cell.getText().equals(s)){
                    return ligneCourante;
                }
            }
            ligneCourante++;
        }
        return -1;
    }
    //charger le fichier csv contenant login mdp
    public ArrayList<Map<String, String>> loadCsvJDD (String nomDuFichier) throws IOException {
        String csvFilePath = "src/main/ressources/JDD/csv/" + nomDuFichier + ".csv";
        ArrayList<Map<String, String>> listJDD = new ArrayList<>();
        List<String[]> list =
                Files.lines(Paths.get(csvFilePath))
                        .map(line -> line.split("\\\\r\\\\n"))
                        .collect(Collectors.toList());
        for (int j = 1; j < list.size(); j++) {
            Map<String, String> jdd = new HashMap<>();
            String[] titres = list.get(0)[0].split(",");
            String[] val = list.get(j)[0].split((","));
            for (int i = 0; i < titres.length; i++) {
                jdd.put(titres[i], val[i]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }


    //écrire ds le fichier
    public static void textFileWriting(String pfile, String ptext) throws IOException {
        try {
            FileOutputStream outputStream = new FileOutputStream(pfile,true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(ptext+" ");
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("Un problème avec le fichier " + pfile);
            throw e;
        }
    }

    //méthode pr mettre en surbrillance l'élément
        public void myClick(WebElement element) throws InterruptedException {
            JavascriptExecutor js=(JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();", element);
            js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
            Thread.sleep(1000);
            element.click();

        }

        public void inputText(WebElement element, String text) throws InterruptedException {
            JavascriptExecutor js=(JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();",element);
            js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
            Thread.sleep(1000);
            element.sendKeys(text);
        }

    }







//    @After
//    public void Quitter() {
//        driver.quit();
//    }
//}
