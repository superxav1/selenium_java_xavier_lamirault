package PoissonCheckBox;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class PoissonCheckBox {

    private WebDriver driver;
    private WebDriverWait wait;
    private String nomDuFichier = "jdd";

    //    private WebElement boutonConnexion, login, password, boutonLogin, boutonFish,
    @Before
    public void creaDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
    }

    @Test
    public void connexionUtilisateur() throws InterruptedException, IOException {

        //récupérer mdp + pwd de la méthode loadCsvJDD
        ArrayList<Map<String, String>> dataList = loadCsvJDD(nomDuFichier);
        Map<String, String> data = dataList.get(0);

        String log = data.get("login");
        String mdp = data.get("password");


        WebElement boutonConnexion = driver.findElement(By.xpath("//div[@id='MenuContent']/a[text()='Sign In']"));
        myClick(boutonConnexion);
        WebElement login = driver.findElement(By.xpath("//input[@name='username']"));
        inputText(login,log);
        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        password.clear();
        inputText(password,mdp);
        WebElement boutonLogin = driver.findElement(By.xpath("//*[@name='signon']"));
        myClick(boutonLogin);

        // L’utilisateur «ABC» est bien connecté (apparition d’un message de bienvenu et du lien «Sign out»)
        WebElement ABC = driver.findElement(By.id("WelcomeContent"));
        String contenuBienvenue = ABC.getText();
        assertEquals("Welcome ABC!", contenuBienvenue);
        assertTrue(driver.findElement(By.xpath("//*[@href='/actions/Account.action?signoff=']")).isDisplayed());
        WebElement boutonFish = driver.findElement(By.xpath("//*[@href='/actions/Account.action?editAccountForm=']"));
        myClick(boutonFish);
        //Affichage de la page de préférence de compte
        // Recherchez l'élément qui contient le texte spécifique
        WebElement element = driver.findElement(By.xpath("//*[contains(text(),'Account Information')]"));

        // Vérifiez si le texte est présent sur la page
        Assert.assertTrue("Le texte 'account information' est présent sur la page.", element.isDisplayed());

        WebElement menu = driver.findElement(By.xpath("//*[@name='account.languagePreference']"));
        Select select = new Select(menu);
        select.selectByValue("japanese");

        WebElement menu2 = driver.findElement(By.xpath("//*[@name='account.favouriteCategoryId']"));
        Select select2 = new Select(menu2);
        select2.selectByValue("REPTILES");

        WebElement MyList = driver.findElement(By.xpath("//input[@name='account.listOption']"));
        WebElement MyBanner = driver.findElement(By.xpath("//input[@name='account.bannerOption']"));

        assertTrue(MyList.isSelected());
        assertTrue(MyBanner.isSelected());

        MyList.click();
        assertFalse(MyList.isSelected());



    }

    public void myClick(WebElement element) throws InterruptedException {
        JavascriptExecutor js=(JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        Thread.sleep(1000);
        element.click();

    }

    public void inputText(WebElement element, String text) throws InterruptedException {
        JavascriptExecutor js=(JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();",element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        Thread.sleep(1000);
        element.sendKeys(text);
    }

    //charger le fichier csv contenant login mdp
    public ArrayList<Map<String, String>> loadCsvJDD (String nomDuFichier) throws IOException {
        String csvFilePath = "src/main/ressources/JDD/csv/" + nomDuFichier + ".csv";
        ArrayList<Map<String, String>> listJDD = new ArrayList<>();
        List<String[]> list =
                Files.lines(Paths.get(csvFilePath))
                        .map(line -> line.split("\\\\r\\\\n"))
                        .collect(Collectors.toList());
        for (int j = 1; j < list.size(); j++) {
            Map<String, String> jdd = new HashMap<>();
            String[] titres = list.get(0)[0].split(",");
            String[] val = list.get(j)[0].split((","));
            for (int i = 0; i < titres.length; i++) {
                jdd.put(titres[i], val[i]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }



//        WebElement ajouterFish = driver.findElement(By.xpath("//*[@href='/actions/Catalog.action?viewProduct=&productId=FI-SW-01']"));
//        ajouterFish.click();
//        WebElement ajouterPanier = driver.findElement(By.xpath("//*[@href='/actions/Cart.action?addItemToCart=&workingItemId=EST-1']"));
//        ajouterPanier.click();
//        driver.findElement(By.xpath("//*[@name='EST-1']")).clear();
//        driver.findElement(By.xpath("//*[@name='EST-1']")).sendKeys("2");
//        driver.findElement(By.xpath("//*[@name='updateCartQuantities']")).click();
//    }
    }


//    @After
//    public void Quitter() {
//        driver.quit();
//    }
//}
