package PoissonCheckBox;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PoissonRowTest {

    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void creaDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
    }

    @Test
    public void testConnexionUtilisateur() {
        // ... Votre code de connexion ici ...

        // Appel à la méthode pour récupérer le numéro de ligne
        int numeroLigne = retournerNumeroDeLigne("K9-BD-01");
        if (numeroLigne != -1) {
            System.out.println("L'élément a été trouvé à la ligne : " + numeroLigne);
        } else {
            System.out.println("L'élément n'a pas été trouvé dans la table.");
        }
    }

    // Méthode pour retourner le numéro de ligne
    public int retournerNumeroDeLigne(String race) {
        int ligneCourante = 1;
        List<WebElement> l_lignes = driver.findElements(By.xpath("//table/tbody/tr"));
        for (WebElement ligne : l_lignes) {
            List<WebElement> l_cell = ligne.findElements(By.xpath("td"));
            for (WebElement cell : l_cell) {
                if (cell.getText().equals(race)) {
                    return ligneCourante;
                }
            }
            ligneCourante++;
        }
        return -1;
    }

    // Vous pouvez ajouter d'autres méthodes ou des méthodes After au besoin.

    // @After
    // public void Quitter() {
    //     driver.quit();
    // }
}
