package Hotelphp;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class AutomhotelDate22222 {

    private WebDriver driver;
    private String url = "http://localhost/TutorialHtml5HotelPhp/";
    private String nomdelaresa = "resa 1";
    private String croix = "//div[contains(text(), 'resa 1')]";
    private String cliquerCroix="//*[@class='scheduler_default_event_delete']";


    @Test
    public void creaDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();

        //verifie la bonne connexion au site
        WebElement verifTitre = driver.findElement(By.xpath("//a[@href='http://code.daypilot.org/27453/html5-hotel-room-booking-javascript-php']"));
        assertEquals("HTML5 Hotel Room Booking (JavaScript/PHP)", verifTitre.getText());

        //Cliquer sur une cellule de la chambre 2
//      WebElement cellule1 = driver.findElement(By.xpath("//div[@class=\"dp\"]/div[3]/div[3]/div/div[2]/div[1]"));
        WebElement cellule1 = driver.findElement(By.xpath("//div[@class='scheduler_default_cell scheduler_default_cell_business' and @style='left: 0px; top: 50px; width: 40px; height: 50px; position: absolute;']"));
        cellule1.click();

        //Aller sur la frame 0
        driver.switchTo().frame(0);
        WebElement resaValid = driver.findElement(By.xpath("//*[@id='f']/h1"));
        assertEquals("New Reservation", resaValid.getText());

        //Remplir la frame 0 et sauvegarder
        WebElement resa1 = driver.findElement(By.id("name"));
        resa1.sendKeys(nomdelaresa);
        driver.findElement(By.id("start")).clear();

        LocalDate dateDuJour = LocalDate.now();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate demain = dateDuJour.plusDays(1);


        //remplir la resa 1 à la date du jour
        driver.findElement(By.id("start")).clear();
        driver.findElement(By.id("start")).sendKeys(dateDuJour.format(formatDate) + "T12:00:00");
        driver.findElement(By.id("end")).clear();
        driver.findElement(By.id("end")).sendKeys(dateDuJour.format(formatDate) + "T12:00:00");

        //sauvegarder la résa
        WebElement save = driver.findElement(By.xpath("//input[@type='submit']"));
        save.click();
        driver.switchTo().defaultContent();

        //cliquer sur la case qui contient "resa 1"
        WebElement cellule2 = driver.findElement(By.xpath("//div[contains(text(), 'resa 1')]"));
        cellule2.click();

        //retourner à la frame de réservation
        driver.switchTo().frame(0);

        //modifier la resa pour la déplacer au lendemain
        driver.findElement(By.id("start")).clear();
        driver.findElement(By.id("start")).sendKeys(demain.format(formatDate) + "T12:00:00");
        driver.findElement(By.id("end")).clear();
        driver.findElement(By.id("end")).sendKeys(demain.format(formatDate) + "T12:00:00");

        WebElement save2 = driver.findElement(By.xpath("//input[@type='submit']"));
        save2.click();
        driver.switchTo().defaultContent();

        //supprimer la résa
        Actions bouger = new Actions(driver);
        bouger.moveToElement(driver.findElement(By.xpath(croix))).build().perform();
        driver.findElement(By.xpath(cliquerCroix)).click();

        //assertion de la suppression
        WebDriverWait wait = new WebDriverWait(driver, 3);
        WebElement msgOK = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Deleted')]")));
        assertTrue(msgOK.isDisplayed());

        //attendre 7 secondes message disparu
        WebDriverWait wait2 = new WebDriverWait(driver, 7);
        boolean msgDisparu = wait2.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(text(),'Deleted')]")));
        assertTrue(msgDisparu);

    }
}