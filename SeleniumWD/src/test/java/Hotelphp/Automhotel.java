package Hotelphp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Automhotel {

    private WebDriver driver;
    private String url = "http://localhost/TutorialHtml5HotelPhp/";
    private String nomdelaresa = "resa 1";
    private String croix = "//div[@class='scheduler_default_event scheduler_default_event_line0']";
    private String cliquerCroix="//div[@style='/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[10]/div/div[4]']";
//    private String cliquerCroix="//*[@class='scheduler_default_event_delete_line0']";


    @Test
        public void creaDriver() {
            System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
            driver = new ChromeDriver();
            driver.get(url);
            driver.manage().window().maximize();

        //verifie la bonne connexion au site
        WebElement verifTitre = driver.findElement(By.xpath("//a[@href='http://code.daypilot.org/27453/html5-hotel-room-booking-javascript-php']"));
        assertEquals("HTML5 Hotel Room Booking (JavaScript/PHP)", verifTitre.getText());

        //Cliquer sur la première cellule du planning
//      WebElement cellule1 = driver.findElement(By.xpath("//div[@class=\"dp\"]/div[3]/div[3]/div/div[2]/div[1]"));
        WebElement cellule1 = driver.findElement(By.xpath("//div[@class='scheduler_default_cell scheduler_default_cell_business' and @style='left: 0px; top: 0px; width: 40px; height: 50px; position: absolute;']"));
        cellule1.click();

        //Aller sur la frame 0
        driver.switchTo().frame(0);
        WebElement resaValid = driver.findElement(By.xpath("//*[@id='f']/h1"));
        assertEquals("New Reservation", resaValid.getText());

        //Remplir la frame 0 et sauvegarder
        WebElement resa1 = driver.findElement(By.xpath("//input[@name='name']"));
        resa1.sendKeys(nomdelaresa);
        WebElement save = driver.findElement(By.xpath("//input[@type='submit']"));
        save.click();

        //revenir sur la page principale
        driver.switchTo().defaultContent();

//        //Vérifier que la resa est bien enregistrée
//        WebElement cellule2 = driver.findElement(By.xpath("//div[@class='scheduler_default_event_inner']"));
//        cellule2.click();
//        driver.switchTo().frame(0);
////        System.out.println(driver.findElement(By.xpath("//input[@value='resa 1']")).getText());
////        assertEquals("resa 1", driver.findElement(By.xpath("//input[@value='resa 1']")).getText());
////        assertEquals(nomdelaresa, driver.findElement(By.id("name")).getText());
//        assertTrue(driver.findElement(By.xpath("//input[@value='" + nomdelaresa + "']")).isDisplayed());
//        driver.switchTo().defaultContent();

        //déplacer la résa au lendemain
        Actions a = new Actions(driver);
        WebElement celluleBase = driver.findElement(By.xpath("//div[@class='scheduler_default_event_inner']"));
        WebElement lendemain = driver.findElement(By.xpath("//*[@id=\"dp\"]/div[3]/div[3]/div/div[2]/div[6]"));
        a.clickAndHold(celluleBase).moveToElement(lendemain).release(lendemain).build().perform();

        //vérifie que le message "Update successful" s'affiche
//        assertTrue(driver.findElement(By.xpath("//div[contains(text(),'Update successful')]")).isDisplayed());
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement msgOK = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Update successful')]")));
        assertTrue(msgOK.isDisplayed());

//        //supprimer la résa
//        Actions bouger = new Actions(driver);
//        bouger.moveToElement(driver.findElement(By.xpath(croix))).build().perform();
//        WebDriverWait waitcroix = new WebDriverWait(driver, 5);
//        WebElement croixrougeapparait = waitcroix.until(ExpectedConditions.presenceOfElementLocated(By.xpath(cliquerCroix)));
//        driver.findElement(By.xpath(cliquerCroix)).click();


    }


}
