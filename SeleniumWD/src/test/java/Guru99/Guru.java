package Hotelphp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class Guru {

    private WebDriver driver;
    private String url = "https://demo.guru99.com/test/newtours/";
    private String loginmdp = "mercury";



    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get(url);
        driver.manage().window().maximize();
    }
    @Test
    public void creaDriver() {
        /* Aller sur la fenêtre de choix des cookies et les accepter

        driver.switchTo().frame("gdpr-consent-notice");
        WebElement accepter = driver.findElement(By.xpath("//button[@id ='save']"));
        accepter.click();

        */

        //aller sur la fenêtre de choix des cookies et les refuser
        driver.switchTo().frame("gdpr-consent-notice");
        WebElement refuser = driver.findElement(By.xpath("//button[@id ='denyAll']"));
        refuser.click();

        //aller sur la nouvelle fenêtre
        WebElement refuser2 = driver.findElement(By.xpath("//button[@class ='mat-focus-indicator okButton mat-raised-button mat-button-base']"));
        refuser2.click();

        //revenir à la fenêtre principale
        driver.switchTo().defaultContent();

        //Rentrer login + mot de passe
        WebElement login = driver.findElement(By.xpath("//input[@name='userName']"));
        login.sendKeys(loginmdp);
        WebElement mdp = driver.findElement(By.xpath("//input[@name='password']"));
        mdp.sendKeys(loginmdp);

        //cliquer sur le bouton submit
        WebElement submit = driver.findElement(By.xpath("//input[@name='submit']"));
        submit.click();

        //vérifie que l'on est bien connecté
        assertEquals("Login Successfully", driver.findElement(By.xpath("//h3[.='Login Successfully']")).getText());
        assertTrue(driver.findElement(By.xpath("//h3[.='Login Successfully']")).isDisplayed());
    }
}
