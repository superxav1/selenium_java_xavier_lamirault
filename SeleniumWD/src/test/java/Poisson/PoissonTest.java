package Poisson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PoissonTest {

    private WebDriver driver;
    private WebDriverWait wait;

    //    private WebElement boutonConnexion, login, password, boutonLogin, boutonFish,
    @Before
    public void creaDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/ressources/driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
    }

    @Test
    public void connexionUtilisateur() {
        WebElement boutonConnexion = driver.findElement(By.xpath("//div[@id='MenuContent']/a[text()='Sign In']"));
        boutonConnexion.click();
        WebElement login = driver.findElement(By.xpath("//input[@name='username']"));
        login.sendKeys("j2ee");
        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        password.clear();
        password.sendKeys("j2ee");
        WebElement boutonLogin = driver.findElement(By.xpath("//*[@name='signon']"));
        boutonLogin.click();

        // L’utilisateur «ABC» est bien connecté (apparition d’un message de bienvenu et du lien «Sign out»)
        WebElement ABC = driver.findElement(By.id("WelcomeContent"));
        String contenuBienvenue = ABC.getText();
        assertEquals("Welcome ABC!", contenuBienvenue);
        assertTrue(driver.findElement(By.xpath("//*[@href='/actions/Account.action?signoff=']")).isDisplayed());

    }

    @Test
    public void listePoisson() {
        WebElement boutonFish = driver.findElement(By.xpath("//*[@id=\"QuickLinks\"]/a[1]"));
        boutonFish.click();
        WebElement ajouterFish = driver.findElement(By.xpath("//*[@href='/actions/Catalog.action?viewProduct=&productId=FI-SW-01']"));
        ajouterFish.click();
        WebElement ajouterPanier = driver.findElement(By.xpath("//*[@href='/actions/Cart.action?addItemToCart=&workingItemId=EST-1']"));
        ajouterPanier.click();
        driver.findElement(By.xpath("//*[@name='EST-1']")).clear();
        driver.findElement(By.xpath("//*[@name='EST-1']")).sendKeys("2");
        driver.findElement(By.xpath("//*[@name='updateCartQuantities']")).click();
    }
}

//    @After
//    public void Quitter() {
//        driver.quit();
//    }
//}
